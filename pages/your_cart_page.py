from module3_1.pages.base_page import BasePage
from selenium.webdriver.common.by import By


class YourCartPage(BasePage):
    inventory_item_name = (By.CSS_SELECTOR, "#item_4_title_link .inventory_item_name")
    continue_shopping_button = (By.ID, "continue-shopping")
    inventory_item_name_2 = (By.CSS_SELECTOR, "#item_0_title_link .inventory_item_name")
    checkout_button = (By.ID, "checkout")

    def text_of_added_item_on_cart_page(self):
        return self.get_text(self.inventory_item_name)

    def text_of_added_item_on_cart_page_2(self):
        return self.get_text(self.inventory_item_name_2)

    def click_continue_shopping_button(self):
        self.click(self.continue_shopping_button)
        from .landing_page import LandingPage
        return LandingPage(self.driver)

    def click_checkout_button(self):
        self.click(self.checkout_button)
