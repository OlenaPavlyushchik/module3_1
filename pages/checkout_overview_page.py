from module3_1.pages.base_page import BasePage
from selenium.webdriver.common.by import By


class CheckoutOverviewPage(BasePage):
    finish_button = (By.ID, "finish")
    price_total_without_taxes = (By.CSS_SELECTOR, "#checkout_summary_container .summary_subtotal_label")

    def click_finish_button(self):
        self.click(self.finish_button)

    def get_price_total(self):
        total_price_text = self.get_text(self.price_total_without_taxes)
        total_price = float(total_price_text.replace('Item total: $', ''))
        return total_price
