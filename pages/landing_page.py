from selenium.webdriver.common.by import By
from module3_1.pages.base_page import BasePage


class LandingPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    add_to_cart_button = (By.XPATH, "//button[@id='add-to-cart-sauce-labs-backpack']")
    shopping_cart_badge = (By.CSS_SELECTOR, "#shopping_cart_container .shopping_cart_badge")
    product_to_be_selected = (By.CSS_SELECTOR, "#inventory_container .inventory_item_name")
    shopping_cart_link = (By.CSS_SELECTOR, "#shopping_cart_container .shopping_cart_link")

    product_to_be_selected_2 = (By.CSS_SELECTOR,
                                "#inventory_container .inventory_item:nth-child(2) .inventory_item_name")
    add_to_cart_button_2 = (By.ID, "add-to-cart-sauce-labs-bike-light")

    price_first_product = (By.CSS_SELECTOR, "#inventory_container .inventory_item_price")
    price_second_product = (By.CSS_SELECTOR, "#inventory_container .inventory_item:nth-child(2) .inventory_item_price")

    def find_add_to_cart_button(self):
        self.find(self.add_to_cart_button)

    def find_text_of_add_to_cart_button(self):
        return self.get_text(self.add_to_cart_button)

    def receive_title(self):
        self.get_title()

    def click_add_to_cart_button(self):
        self.click(self.add_to_cart_button)

    def find_text_of_shopping_cart_badge(self):
        return self.get_text(self.shopping_cart_badge)

    def text_of_selected_product(self):
        return self.get_text(self.product_to_be_selected)

    def text_of_selected_product_2(self):
        return self.get_text(self.product_to_be_selected_2)

    def click_add_to_cart_button_second_product(self):
        self.click(self.add_to_cart_button_2)

    def click_shopping_cart_link(self):
        self.click(self.shopping_cart_link)
        from .your_cart_page import YourCartPage
        return YourCartPage(self.driver)

    def get_first_product_price(self):
        price_text = self.get_text(self.price_first_product)
        price = float(price_text.replace('$', ''))
        return price

    def get_second_product_price(self):
        price_text = self.get_text(self.price_second_product)
        price = float(price_text.replace('$', ''))
        return price
