from module3_1.pages.base_page import BasePage
from selenium.webdriver.common.by import By


class CompletePage(BasePage):
    complete_message = (By.CSS_SELECTOR, "#checkout_complete_container .complete-header")

    def get_text_of_complete_message(self):
        return self.get_text(self.complete_message)
