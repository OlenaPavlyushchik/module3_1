from selenium.webdriver.common.by import By
from module3_1.pages.base_page import BasePage
from module3_1.pages.landing_page import LandingPage


class HomePage(BasePage):
    username_field = (By.ID, "user-name")
    password_field = (By.ID, "password")
    login_button = (By.ID, "login-button")
    error_message_field = (By.XPATH, "//div[@class='error-message-container error']//h3[@data-test='error']")

    def __init__(self, driver):
        super().__init__(driver)

    def set_username(self, username):
        self.set(self.username_field, username)

    def set_password(self, password):
        self.set(self.password_field, password)

    def click_login_button(self):
        """Transition method which results in opening new Landing page"""
        self.click(self.login_button)
        return LandingPage(self.driver)

    def log_into_application(self, username, password):
        """This is convenience method which combines few above methods"""
        self.set_username(username)
        self.set_password(password)
        self.click_login_button()

    def get_error_message(self):
        return self.get_text(self.error_message_field)
