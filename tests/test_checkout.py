from module3_1.pages.home_page import HomePage
from module3_1.tests.base_test import BaseTest
from module3_1.pages.landing_page import LandingPage
from module3_1.pages.your_cart_page import YourCartPage
from module3_1.pages.checkout_your_info_page import CheckoutInfoPage
from module3_1.pages.checkout_overview_page import CheckoutOverviewPage
from module3_1.pages.complete_page import CompletePage
import os


class TestCheckout(BaseTest):
    """Purpose of the Test Case: The test verifies that a standard user can successfully add two products to their
    shopping cart and go through the checkout process."""
    def test_standard_checkout(self):

        """Login in:
        Login: The test initiates by logging in to the application. The HomePage page object is used to set the username
        and password. The user clicks the login button, which leads to the landing page."""

        home_page = HomePage(self.driver)
        home_page.set_username(os.environ.get('USERNAME'))
        home_page.set_password(os.environ.get('PASSWORD'))
        landing_page = home_page.click_login_button()

        """Adding first product:
        The LandingPage page object is used to select the first product to add to the cart. 
        The name and price of the product are saved for later comparisons. 
        After adding the first product, it checks that the shopping cart badge is updated to reflect a total of one 
        product. 
        The cart page is then opened and it checks that the name of the product in the cart matches the name of the 
        added product"""

        actual_text_of_product_to_select = landing_page.text_of_selected_product()
        price1 = landing_page.get_first_product_price()
        landing_page.click_add_to_cart_button()
        number_of_products_added = landing_page.find_text_of_shopping_cart_badge()
        assert number_of_products_added == "1"
        landing_page.click_shopping_cart_link()
        your_cart_page = YourCartPage(self.driver)
        actual_name_of_added_item_on_cart_page = your_cart_page.text_of_added_item_on_cart_page()
        assert actual_name_of_added_item_on_cart_page == actual_text_of_product_to_select

        """Adding second product:
        The test further repeats the process with a second product. After adding the second product, it again checks 
        the shopping cart badge (which should now reflect a total of 2 products), opens the cart and checks that the 
        name of the second product in the cart matches the name of the second added product."""

        your_cart_page.click_continue_shopping_button()
        landing_page = LandingPage(self.driver)
        actual_text_of_product_to_select_2 = landing_page.text_of_selected_product_2()
        price2 = landing_page.get_second_product_price()
        landing_page.click_add_to_cart_button_second_product()
        number_of_products_added = landing_page.find_text_of_shopping_cart_badge()
        assert number_of_products_added == "2"
        landing_page.click_shopping_cart_link()
        your_cart_page = YourCartPage(self.driver)
        actual_name_of_added_item_on_cart_page_2 = your_cart_page.text_of_added_item_on_cart_page_2()
        assert actual_name_of_added_item_on_cart_page_2 == actual_text_of_product_to_select_2

        """Filling in user information:
        The test then begins the checkout process. The CheckoutInfoPage page object is used to fill in user information 
        (first name, last name, postal code), which are fetched from environment variables."""

        your_cart_page.click_checkout_button()
        checkout_your_information_page = CheckoutInfoPage(self.driver)
        checkout_your_information_page.set_first_name(os.environ.get('FIRST_NAME'))
        checkout_your_information_page.set_last_name(os.environ.get('LAST_NAME'))
        checkout_your_information_page.set_postal_code(os.environ.get('POST_CODE'))
        checkout_your_information_page.click_continue_button()
        checkout_overview_page = CheckoutOverviewPage(self.driver)

        """"Total price check:
        The total price of both added products is stored price_for_two_products. This value is compared against 
        the total price sum up, verifying that the calculations match."""

        price_for_two_products = checkout_overview_page.get_price_total()
        assert price_for_two_products == price1 + price2

        """Completion of order:
        The CheckoutOverviewPage page object is used to finalize the order. The user clicks "Finish," completing the 
        order and leading to the CompletePage. The test then verifies that the order confirmation message displayed 
        matches the expected message "Thank you for your order!"""

        checkout_overview_page.click_finish_button()
        order_complete_page = CompletePage(self.driver)
        actual_message_on_completion = order_complete_page.get_text_of_complete_message()
        assert actual_message_on_completion == "Thank you for your order!"
