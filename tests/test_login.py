from module3_1.pages.home_page import HomePage
from module3_1.tests.base_test import BaseTest
from module3_1.pages.landing_page import LandingPage
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
import os


class TestLogin(BaseTest):
    def test_valid_credentials(self):
        home_page = HomePage(self.driver)
        home_page.set_username(os.environ.get('USERNAME'))
        home_page.set_password(os.environ.get('PASSWORD'))
        home_page.click_login_button()
        landing_page = LandingPage(self.driver)
        actual_title_on_landing_page = landing_page.get_title()
        assert actual_title_on_landing_page == "Swag Labs"
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located(LandingPage.add_to_cart_button))
        actual_text_on_add_to_cart_button = landing_page.find_text_of_add_to_cart_button()
        assert actual_text_on_add_to_cart_button == "Add to cart"

    def test_valid_visual_user(self):
        home_page = HomePage(self.driver)
        home_page.set_username(os.environ.get('VALID_VISUAL_USER'))
        home_page.set_password(os.environ.get('VALID_VISUAL_USER_PASSWORD'))
        home_page.click_login_button()
        landing_page = LandingPage(self.driver)
        actual_title_on_landing_page = landing_page.get_title()
        assert actual_title_on_landing_page == "Swag Labs"
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located(LandingPage.add_to_cart_button))
        actual_text_on_add_to_cart_button = landing_page.find_text_of_add_to_cart_button()
        assert actual_text_on_add_to_cart_button == "Add to cart"

    def test_invalid_credentials(self):
        home_page = HomePage(self.driver)
        home_page.log_into_application(
          "Invalid Email", "Invalid Password")
        actual_message = home_page.get_error_message()
        assert actual_message == "Epic sadface: Username and password do not match any user in this service"

    def test_empty_credentials(self):
        home_page = HomePage(self.driver)
        home_page.log_into_application(
          "", "")
        actual_message = home_page.get_error_message()
        assert actual_message == "Epic sadface: Username is required"

    def test_empty_password(self):
        home_page = HomePage(self.driver)
        home_page.log_into_application(
          "standard_user", "")
        actual_message = home_page.get_error_message()
        assert actual_message == "Epic sadface: Password is required"

    def test_locked_out_user(self):
        home_page = HomePage(self.driver)
        take_username = os.environ.get('LOCKED_OUT_USER')
        take_password = os.environ.get('LOCKED_OUT_USER_PASSWORD')
        home_page.log_into_application(take_username, take_password)
        actual_message = home_page.get_error_message()
        assert actual_message == "Epic sadface: Sorry, this user has been locked ou."
