## Description

The repository contains tests for testing successful and unsuccessful scenarios of login:
The tests will be run on 3 browsers: Chrome, Mozilla and Edge
Test are the following:
1.Test login with valid standard user credentials
2.Test login with valid visual user credentials
3.Test login with invalid credentials
4.Test login with empty credentials
5.Test login with empty password
6.Test login with locked out user

Another test is for completion of checkout by the user.

## Installation and execution

First install dependencies:
```sh
pip install -r requirements.txt
```
Clone project from git to your local: select 'Clone with SSH key', copy URL
git clone 'URL'

In project root folder create pytest.ini file adding env variables: 
test data for users, user's information

To run tests:
```sh
cd module3_1
pytest
```
To generate report:
```sh
cd module3_1
pytest --html=report.html
```
find generated report.html in module3_1 root folder > Open In > Browser

