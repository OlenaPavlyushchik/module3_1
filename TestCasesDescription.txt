Test Cases for Login

Test Case ID: TC001
Description: Test login with valid standard user credentials
Prerequisite: None
Steps:  
1. Go to the login page
2. Enter valid standard user username and password
3. Click on Login button
Expected Result: The user is redirected to the main page. The title of the landing page is "Swag Labs" and "Add to cart" button is visible.

Test Case ID: TC002
Description: Test login with valid visual user credentials
Prerequisite: None
Steps:  
1. Go to the login page
2. Enter valid visual user username and password
3. Click on Login button
Expected Result: The visual user is redirected to the main page. The title of the landing page is "Swag Labs" and "Add to cart" button is visible.

Test Case ID: TC003
Description: Test login with invalid credentials
Prerequisite: None
Steps:  
1. Go to the login page
2. Enter invalid username and password
3. Click Login button
Expected Result: The user is not able to login and the error message appears: "Epic sadface: Username and password do not match any user in this service".

Test Case ID: TC004
Description: Test login with empty credentials
Prerequisite: None
Steps:  
1. Go to the login page
2. Leave username and password fields empty
3. Click Login button
Expected Result: User is not able to login and error message "Epic sadface: Username is required" is displayed.

Test Case ID: TC005
Description: Test login with empty password
Prerequisite: None
Steps:  
1. Go to the login page
2. Enter valid username but leave the password field empty
3. Click Login button
Expected Result: User is not able to login and error message "Epic sadface: Password is required" is displayed.

Test Case ID: TC006
Description: Test login with locked out user
Prerequisite: None
Steps:  
1. Go to the login page
2. Enter locked out user's username and password
3. Click Login button
Expected Result: User is not able to login and error message "Epic sadface: Sorry, this user has been locked out." is displayed.
checking

Test Case for Checkout:
1. Login: User login with valid credentials.
2. First Product Addition: User selects and adds first product to the cart, verifies its addition.
3. Second Product Addition: User selects and adds second product to the cart, verifies its addition.
4. Checkout Info Entry: User provides necessary checkout information, namely: first name, last name, and postal code.
5. Total Price Verification: Confirm that the computed total price of both products in the cart matches the displayed total price.
6. Order Completion: Confirm that after clicking "Finish," the order completion message is correctly displayed with "Thank you for your order!"
Expected Result: Correct confirmation message is displayed
